import React from 'react';

import { Box, Grid, Skeleton } from '@mui/material';
import { dispatch, useSelector } from '../../../store';
import { getLoanSlipWaiting } from '../../../store/slices/loanSlip';
import { getRegisterSlipWaiting } from '../../../store/slices/registerSlip';
import { getStateOfBookWaiting } from '../../../store/slices/stateOfBook';
import { getSuppliesWaiting } from '../../../store/slices/supplies';
import { getTopStandingWaiting } from '../../../store/slices/topStanding';
import { GetLoanSlip } from '../../../types/loanSlip';
import { GetSupplies } from '../../../types/supplies';
import { GetTopStanding } from '../../../types/topStanding';
import StickyHeadTable from './table';

export default function HomePage() {
    const [dataStanding, setDataStanding] = React.useState<GetTopStanding[] | undefined>([]);
    const [dataLoanSlip, setDataLoanSlip] = React.useState<GetLoanSlip[] | undefined>([]);
    const [dataSupplies, setDataSupplies] = React.useState<GetSupplies[] | undefined>([]);

    const { getTopStanding } = useSelector((state) => state.getTopStanding);
    const { getSupplies } = useSelector((state) => state.getSupplies);
    const { getLoanSlip } = useSelector((state) => state.getLoanSlip);
    const { getStateOfBook } = useSelector((state) => state.getStateOfBook);
    const { getRegisterSlip } = useSelector((state) => state.getRegisterSlip);

    React.useEffect(() => {
        setDataSupplies(getSupplies);
        setDataStanding(getTopStanding);
        setDataLoanSlip(getLoanSlip);
    }, [getTopStanding, getSupplies, getLoanSlip]);

    React.useEffect(() => {
        dispatch(getTopStandingWaiting());
        dispatch(getSuppliesWaiting());
        dispatch(getLoanSlipWaiting());
        dispatch(getStateOfBookWaiting());
        dispatch(getRegisterSlipWaiting());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div>

            {dataSupplies?.length !== 0 && dataLoanSlip?.length !== 0 && dataStanding?.length !== 0 && dataSupplies !== undefined && dataLoanSlip !== undefined && dataStanding !== undefined && getRegisterSlip !== undefined ?
                <StickyHeadTable dataStanding={dataStanding} dataSupplies={dataSupplies} dataLoanSlip={dataLoanSlip} dataStateOfBook={getStateOfBook} dataRegisterSlip={getRegisterSlip} />
                :
                <>
                    <Box sx={{ flexGrow: 1, height: 'auto' }}>
                        <Grid container md={12} justifyContent='space-between' gap={{ xs: 2, height: 'auto' }}>
                            <Grid container lg={3} md={3} sm={12} xs={12} justifyContent='flex-end' gap={{ xs: 2 }}>
                                <Grid lg={12} xs={12}>
                                    <Skeleton variant="rectangular" width='100%' height={80} sx={{ borderRadius: 5 }} />
                                </Grid>
                                <Grid lg={12} xs={12}>
                                    <Skeleton variant="rectangular" width='100%' height={80} sx={{ borderRadius: 5 }} />
                                </Grid>
                                <Grid lg={12} xs={12}>
                                    <Grid mt={1} lg={12} xs={12} container gap={1} justifyContent='center'>
                                        <Skeleton variant="rectangular" width='100%' height={80} sx={{ borderRadius: 5 }} />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid container md={8.5} sm={12} xs={12} alignItems='flex-start'>
                                <Skeleton variant="rectangular" width='100%' height={500} sx={{ borderRadius: 5 }} />
                            </Grid>
                        </Grid>
                    </Box>
                </>
            }
        </div>
    );
}
