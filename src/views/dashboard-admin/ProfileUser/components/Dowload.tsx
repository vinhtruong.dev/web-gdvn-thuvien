import React from 'react';

const DownloadButton: React.FC = () => {
  return (
    <button>
      <a href='https://drive.google.com/drive/folders/10LZEB_gqJvGW_IHXotONHVmAMWM-7i26?usp=sharing' target="_blank" rel="noreferrer">Tải xuống</a>
    </button>
  );
};

export default DownloadButton;