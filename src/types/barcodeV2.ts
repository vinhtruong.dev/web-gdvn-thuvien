// material-ui
import { AlertProps, SnackbarOrigin } from '@mui/material';

// ==============================|| SNACKBAR TYPES  ||============================== //

export interface BarcodeProps {
    openBC: boolean,
    closeBC: boolean,
    userID: string,
}
