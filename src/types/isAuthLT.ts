export interface IsAuthLTStateProps {
  isLogin?: boolean;
  error?: object | string | null;
}
